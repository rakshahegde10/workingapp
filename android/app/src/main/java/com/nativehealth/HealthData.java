package com.nativehealth;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class HealthData implements Serializable {

        private String steps,heartRate,systolic, diastolic, bloodGlucose,bodyTemperature,age, startDate, endDate, dataType;
        JSONObject jsonObject = new JSONObject();

        public void Steps() {
            this.dataType = dataType;
            this.steps = steps;
            this.startDate = startDate;
            this.endDate = endDate;
        }


        public void HeartRate() {
            this.dataType = dataType;
            this.heartRate = heartRate;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public void BloodPressure(String type, String sys, String dia, String sdate, String edate) {
            this.dataType = type;
            this.systolic = sys;
            this.diastolic = dia;
            this.startDate = sdate;
            this.endDate = edate;
            try {

                jsonObject.put("dataType", dataType);
                jsonObject.put("SysValue", systolic);
                jsonObject.put("SysValue", diastolic);
                jsonObject.put("startDate", startDate);
                jsonObject.put("endDate", endDate);
            } catch (Exception e) {
                Log.i("error", "error!!!!obj");
                Log.v(e.toString(), null);

            }
        }
        
        public JSONObject getBloodPressure() throws JSONException {
            try {
                return jsonObject;
            } catch (Exception e) {
                Log.i("error", "error!!!!obj");
                Log.v(e.toString(), null);
                return null;
            }
        }


        public void BloodGlucose() {
            this.dataType = dataType;
            this.bloodGlucose = bloodGlucose;
            this.startDate = startDate;
            this.endDate = endDate;
        }


        public void BodyTemperature() {
            this.dataType = dataType;
            this.bodyTemperature = bodyTemperature;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public void Age() {
            this.age = age;
        }

}
